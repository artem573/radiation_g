import math
import numpy as np
import matplotlib.pyplot as plt

def GaussSeidel():
    # Gauss Seidel subiterations
    for GS_i in range(0,N_GS_iter):
        for i in range(1,Nx-1):
            G_n[i] = (-a[i]*G[i-1] - c[i]*G[i+1] + d[i])/b[i]
            G[i] = G[i] + alfa_G*(G_n[i] - G[i])

        # dG/dx = 0 at x = Lx; c = 0, b = -a+kappa
        G_n[Nx-1] = (-a[Nx-1]*G[Nx-2] + d[Nx-1])/(-a[Nx-1]+kappa[Nx-1])
        G[Nx-1] = G[Nx-1] + alfa_G*(G_n[Nx-1] - G[Nx-1])

        for i in range(Nx-2,0,-1):
            G_n[i] = (-a[i]*G[i-1] - c[i]*G[i+1] + d[i])/b[i]
            G[i] = G[i] + alfa_G*(G_n[i] - G[i])


def Progonka():

    eps_coeff = epsilon_w/(2.0*(2.0 - epsilon_w))
    diff_by_dx = 1.0/(3.0*kappa[1]*dx)
    P[0] = -diff_by_dx/(diff_by_dx + eps_coeff)
    Q[0] = eps_coeff/(diff_by_dx + eps_coeff)*4.0*sigma*T[0]**4
    
    for i in range(1,Nx):
        P[i] = -c[i]/(b[i] + a[i]*P[i-1])
        Q[i] = (d[i] + a[i]*Q[i-1])/(b[i] + a[i]*P[i-1])

    eps_coeff = epsilon_w/(2.0*(2.0 - epsilon_w))
    diff_by_dx = 1.0/(3.0*kappa[Nx-1]*dx)
    f = diff_by_dx/(diff_by_dx + eps_coeff)
    G_n[Nx] = (f*Q[Nx-1] + (1.0-f)*4.0*sigma*T[Nx]**4)/(1+f*P[Nx-1])
    G_n[Nx-1] = Q[Nx-1] - P[Nx-1]*G_n[Nx]

    # G_n[Nx-1] = Q[Nx-1]/(1.0+P[Nx-1])

    for i in range(Nx-2,-1,-1):
        G_n[i] = Q[i] - P[i]*G_n[i+1]

    # Relax
    for i in range(0,Nx):
        G[i] = G[i] + alfa_G*(G_n[i] - G[i])

            
epsilon_w = 1.0
sigma = 5.67e-8

Lx = 0.014
Nx = 1000
dx = Lx/Nx
N_iter = 10
N_write = 1
alfa_G = 1.0
N_GS_iter = 4

T0 = 300.0
TN = 3000.0

G = np.zeros(Nx+1)
G_n = np.zeros(Nx+1)
Qr = np.zeros(Nx+1)
kappa = np.zeros(Nx+1)
Gamma_f = np.zeros(Nx+1)
T = np.zeros(Nx+1)
Y_F = np.zeros(Nx+1)
Y_O = np.zeros(Nx+1)
Y_P = np.zeros(Nx+1)

P = np.zeros(Nx+1)
Q = np.zeros(Nx+1)
a = np.zeros(Nx+1)
c = np.zeros(Nx+1)
b = np.zeros(Nx+1)
d = np.zeros(Nx+1)

# Read T, Yi fields
with open("T_s") as f_in:
    line = f_in.readline()
#    T[0] = T0
    T[0] = float(line)
    
with open("T") as f_in:
    for i in range(1,Nx+1):
        line = f_in.readline()
#        T[i] = T[0] + (TN - T0)*i/Nx
        T[i] = float(line)
        
with open("Y_F") as f_in:
    for i in range(1,Nx+1):
        line = f_in.readline()
        Y_F[i] = float(line)

with open("Y_O") as f_in:
    for i in range(1,Nx+1):
        line = f_in.readline()
        Y_O[i] = float(line)

with open("Y_P") as f_in:
    for i in range(1,Nx+1):
        line = f_in.readline()
        Y_P[i] = float(line)


for i in range(0,Nx+1):
    G[i] = sigma*T[i]**4


for i in range(1,Nx):
    T_F = T[i]
    T_P = 1.0/T[i]
    
    kappa_O = 0.01
    kappa_N = 0.01
    kappa_F = 6.63 - 3.57e-3*T_F + 1.68e-8*T_F*T_F + 2.56e-10*T_F*T_F*T_F + 2.66e-14*T_F*T_F*T_F*T_F + 0
    kappa_P = 16.51 - 6.98e4*T_P + 1.34e8*T_P*T_P - 8.74e10*T_P*T_P*T_P + 2.43e13*T_P*T_P*T_P*T_P - 2.43e15*T_P*T_P*T_P*T_P*T_P

    Y_N = 1.0 - Y_F[i] - Y_O[i] - Y_P[i]

#    kappa[i] = 1.0
    kappa[i] = Y_F[i]*kappa_F + Y_O[i]*kappa_O + Y_P[i]*kappa_P + Y_N*kappa_N

kappa[0] = kappa[1]
kappa[Nx] = kappa[Nx-1]

Gamma_f[0] = 1.0/(3.0*kappa[0])
for i in range(1,Nx):
    Gamma_f[i] = 0.5*(1.0/(3.0*kappa[i]) + 1.0/(3.0*kappa[i+1]))


for i in range(1,Nx):
    a[i] = Gamma_f[i-1]/(dx*dx)
    c[i] = Gamma_f[i]/(dx*dx)
    b[i] = (a[i]+c[i])+kappa[i]



    
print " ========================================================================= "
with open("out","w+") as f_out:
    for it in range(1,N_iter+1):
        for i in range(1,Nx):
            d[i] = kappa[i]*4.0*sigma*T[i]**4 #- kappa[i]*G[i]

        # Solve
        Progonka()
#        GaussSeidel()

        # Boundary x = 0
        eps_coeff = epsilon_w/(2.0*(2.0 - epsilon_w))
        diff_by_dx = 1.0/(3.0*kappa[1]*dx)
        G[0] = G[1] * diff_by_dx/(diff_by_dx+eps_coeff) + 4.0*sigma*T[0]**4 * eps_coeff/(diff_by_dx+eps_coeff)

        

        # Boundary dG/dx = 0 at x = Lx
        eps_coeff = epsilon_w/(2.0*(2.0 - epsilon_w))
        diff_by_dx = 1.0/(3.0*kappa[Nx-1]*dx)
        G[Nx] = G[Nx-1] * diff_by_dx/(diff_by_dx+eps_coeff) + 4.0*sigma*T[Nx]**4 * eps_coeff/(diff_by_dx+eps_coeff)
#        G[Nx] = G[Nx-1]

        for i in range(1,Nx):
            Qr[i] = -1.0/(3.0*kappa[i]) * (G[i+1] - 2*G[i] + G[i-1])/dx/dx

        q_w_r_1 = -1.0/(3.0*kappa[1])*(G[1]-G[0])/dx
        q_w_r_2 = eps_coeff*(4.0*sigma*T[0]**4 - G[0])
        q_w_r_out_1 = -1.0/(3.0*kappa[Nx-1])*(G[Nx]-G[Nx-1])/dx
        q_w_r_out_2 = eps_coeff*(4.0*sigma*T[Nx]**4 - G[Nx])

        if(it % N_write == 0):
            print "iteration =", it

            Qr_sum_1 = 0.0
            Qr_sum_2 = 0.0
            for i in range(1,Nx):
                Qr_sum_1 += Qr[i]*dx
                Qr_sum_2 += kappa[i]*(4.0*sigma*T[i]**4 - G[i])*dx
                        
            f_out.write("iteration = " + str(it) + " q_w_r_1 = " + '{:12.5e}'.format(q_w_r_1) + " q_w_r_2 = " + '{:12.5e}'.format(q_w_r_2) + " sum(Qr_1) = " + '{:12.5e}'.format(Qr_sum_2) + " sum(Qr) = " + '{:12.5e}'.format(Qr_sum_2) + " q_w_r_out_1 = "+ '{:12.5e}'.format(q_w_r_out_1) + " q_w_r_out_2 = "+ '{:12.5e}'.format(q_w_r_out_2) + '\n')
            f_out.write("x_i  " + "   G[i]   " + "        4*sigma*T[i]**4   " + "    Qr[i]   " + " d[i]-kappa[i]*G[i]" + "       kappa[i]  " + '\n')
            for i in range(0,Nx+1):
                f_out.write('{:3d}'.format(i) + '{:16.5e}'.format(G[i]) + '{:16.5e}'.format(4.0*sigma*T[i]**4) + '{:16.5e}'.format(Qr[i]) + '{:16.5e}'.format(d[i]-kappa[i]*G[i]) + '{:16.5e}'.format(kappa[i]) + '\n')

#            plt.plot(G)


 
# plt.plot(kappa)
# plt.show()

print " ========================================================================= "
